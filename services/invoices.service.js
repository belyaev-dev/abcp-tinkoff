"use strict";

const DbMixin = require("../mixins/db.mixin");
const axios = require("axios");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "invoices",
	version: 1,

	/**
	 * Mixins
	 */
	mixins: [DbMixin("invoices")],

	/**
	 * Settings
	 */
	settings: {
		// Available fields in the responses
		fields: [
			"_id",
			"operationId",
			"date",
			"amount",
			"payerName",
			"payerInn",
			"payerBank",
			"paymentPurpose"
		],

		// Validator for the `create` & `insert` actions.
		entityValidator: {
			// accountNumber: "int|positive"
		}
	},

	/**
	 * Action Hooks
	 */
	hooks: {
		before: {
			/**
			 * Register a before hook for the `create` action.
			 * It sets a default value for the quantity field.
			 *
			 * @param {Context} ctx
			 */
			create(ctx) {
				// ctx.params.partner_id = 0;
				// ctx.params.account_id = 0;
				// ctx.params.disabled = 0;
			}
		}
	},

	/**
	 * Actions
	 */
	actions: {

		/**
		 * The "moleculer-db" mixin registers the following actions:
		 *  - list
		 *  - find
		 *  - count
		 *  - create
		 *  - insert
		 *  - update
		 *  - remove
		 */

		// --- ADDITIONAL ACTIONS ---

		/**
		 * Increase the quantity of the product item.
		 */
		findUser: {
			rest: "GET /send/:id",
			params: {
				id: "string",
				// inn: "number|integer|positive"
			},
			async handler(ctx) {
				console.log(ctx.params.id);
				const inv = await this.actions.find({query: {operationId: ctx.params.id }});
				console.log("invoice:",inv);
				if(inv.length != 0){
					return await ctx.call("abcp.paymentNew.async", {params: inv[0]});
				} else return "Operation not found";

			}
		},

		// /**
		//  * Decrease the quantity of the product item.
		//  */
		// decreaseQuantity: {
		// 	rest: "PUT /:id/quantity/decrease",
		// 	params: {
		// 		id: "string",
		// 		value: "number|integer|positive"
		// 	},
		// 	/** @param {Context} ctx  */
		// 	async handler(ctx) {
		// 		const doc = await this.adapter.updateById(ctx.params.id, { $inc: { quantity: -ctx.params.value } });
		// 		const json = await this.transformDocuments(ctx, ctx.params, doc);
		// 		await this.entityChanged("updated", json, ctx);

		// 		return json;
		// 	}
		// }
	},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Loading sample data to the collection.
		 * It is called in the DB.mixin after the database
		 * connection establishing & the collection is empty.
		 */
		// async seedDB() {
		// 	await this.adapter.insertMany([
		// 		{ subdomain: "tigrenok", partner_id: 0, account_id: 704, disabled: 0 },
		// 		{ subdomain: "investologia", partner_id: 25, account_id: 999, disabled: 0 },
		// 		{ subdomain: "gordischool", partner_id: 15, account_id: 679, disabled: 1},
		// 	]);
		// }
	},

	/**
	 * Fired after database connection establishing.
	 */
	async afterConnected() {
		// await this.adapter.collection.createIndex({ name: 1 });
	}
};
