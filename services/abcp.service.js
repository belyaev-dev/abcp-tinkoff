"use strict";

const axios = require("axios");
const fs = require("fs");
const qs = require("qs");
const {
	DateTime
} = require("luxon");
const QueueMixin = require("moleculer-rabbitmq");
const {
	MoleculerError
} = require("moleculer").Errors;

const queueMixin = QueueMixin({
	connection: "amqp://admin:abcponelove@rabbitmq:5672",
	asyncActions: true, // Enable auto generate .async version for actions
});

const set = {
	userlogin: "api@id9686",
	userpsw: "8e77b37aef29e5d550f12cd731bce9d4"
};

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "abcp",
	mixins: [queueMixin],
	logger: "File",
	/**
	 * Settings
	 */
	settings: {

	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Create new payment
		 *
		 * @returns
		 */
		paymentNew: {
			rest: {
				path: "/payment/new"
			},
			params: {
				// inn: "string"
			},
			queue: {
				retryExchangeAssert: { // RabbitMQ advance exchange option
					durable: true, // (boolean) if true, the exchange will survive broker restarts. Defaults to true.
					autoDelete: false, // (boolean) if true, the exchange will be destroyed once the number of bindings for which it is the source drop to zero. Defaults to false.
					alternateExchange: null, // (string) an exchange to send messages to if this exchange can’t route them to any queues.
					arguments: { // additional arguments, usually parameters for some kind of broker-specific extension e.g., high availability, TTL
					},
				},
				retry: {
					max_retry: 3, // Max retry count, 3 mean if the first time failed, it will try 3 more times
					delay: (retry_count) => { // Number of miliseconds delay between each retry, could be a number or a function(retry_count) that return a number
						console.log("retry:", retry_count);
						return 1800000;
					},
					// delay: 30000
				},
			},
			/** @param {Context} ctx  */
			async handler(ctx) {
				// await this.actions.userSync();
				const user = await ctx.call("v1.users.find", {
					query: {
						inn: ctx.params.payerInn
					}
				});
				if (user.length != 0) {
					console.log("undefined!", user, user.length);
					let dt = DateTime.now().setZone("Asia/Novosibirsk").toLocaleString(DateTime.TIME_24_WITH_SECONDS);
					console.log(dt);
					let response = await axios
						.post("https://id9686.public.api.abcp.ru/cp/finance/payments", qs.stringify({
							userlogin: set.userlogin,
							userpsw: set.userpsw,
							payments: [{
								userId: user[0].userId,
								createDateTime: ctx.params.date + ` ${dt}`,
								paymentTypeId: 14923,
								amount: ctx.params.amount,
								comment: `${ctx.params.paymentPurpose} // ${ctx.params.payerName}`
							}],
							linkPayments: 0
						}))
						.then(res => res.data)
						.catch(error => {
							console.error(error);
							let msg = `💰 Входящий платеж от клиента ${ctx.params.payerName} (${ctx.params.payerInn})\n` +
							`Дата платежа: ${ctx.params.date}\n` +
							`Назначение: ${ctx.params.paymentPurpose}\n` +
							`Сумма: ${ctx.params.amount}\n\n` +
							`🆘 Не удалось записать платеж!\n` +
							`Подробности: ${JSON.stringify(error.response.data)}\n` +
							`Повторить отправку: http://getamo.online:3000/api/v1/invoices/send/${ctx.params.operationId}\n`;
							axios.post("https://api.telegram.org/bot1992463272:AAGl2ptLKNGAJN4kcKrTZB8pB6Cz8FfP0G8/sendMessage", {
								chat_id: "-559336176",
								text: msg
							});
							return error;
						});
					console.log("response:", response);
					this.logger.info("Payment created. ", response);
				} else {
					// console.log(user.length);
					// console.log("user",user);
					let msg = `💰 Входящий платеж от клиента ${ctx.params.payerName} (${ctx.params.payerInn})\n` +
						`Дата платежа: ${ctx.params.date}\n` +
						`Назначение: ${ctx.params.paymentPurpose}\n` +
						`Сумма: ${ctx.params.amount}\n\n` +
						`⚠️ Не найден ИНН в abcp\n` +
						`Повторить отправку: http://getamo.online:3000/api/v1/invoices/send/${ctx.params.operationId}\n`;
					axios.post("https://api.telegram.org/bot1992463272:AAGl2ptLKNGAJN4kcKrTZB8pB6Cz8FfP0G8/sendMessage", {
						chat_id: "-559336176",
						text: msg
					});
					this.logger.error("User not found. ", ctx.params);
					throw new MoleculerError("User not found after 3 retries", 404, "ERR_NOT_FOUND", {
						inn: ctx.params.payerInn,
					});

				}
			},
		},
		userFind: {
			rest: {
				path: "/user/:inn"
			},
			params: {
				inn: "number|integer|positive"
			},
			/** @param {Context} ctx  */
			async handler(ctx) {
				return "Hello Moleculer";
			}
		},
		userGet: {
			rest: {
				path: "/user/get"
			},
			params: {},
			/** @param {Context} ctx  */
			async handler(ctx) {
				const offset = parseInt(fs.readFileSync("./offset_tdavto.json", "utf8"));
				console.log(`offset: ${offset}`);

				const users = await axios
					.get("https://id9686.public.api.abcp.ru/cp/users", {
						params: {
							userlogin: set.userlogin,
							userpsw: set.userpsw,
							skip: offset
						}
					})
					.then((res) => res.data)
					.catch((error) => {
						console.error(error);
						return error;
					});

				console.log("count: %d", users.length);

				if (users.length == 1000) {
					fs.writeFileSync(
						"./offset_tdavto.json",
						`${offset + users.length}`,
						"utf8"
					);
					//* works on NeDB
					ctx.call("v1.users.create", users);
					// ctx.call("v1.users.insert", users);
					axios.get(`https://api.telegram.org/bot1992463272:AAGl2ptLKNGAJN4kcKrTZB8pB6Cz8FfP0G8/sendMessage?chat_id=-559336176&text=Import%20in%20process.%20\nCurrent:%20${offset+users.length}`);
				} else {
					let currTime, lastTime = 0;
					const userPromises = users.map(async (user) => {
						const result = await ctx.call("v1.users.find", {
							query: {
								userId: user.userId
							}
						});

						if (result == 0) {
							console.log("user not found");
							ctx.call("v1.users.create", user);
						} else console.log(`user found: ${user.updateTime}`);
						currTime = new Date(user.updateTime).getTime();
						if (currTime > lastTime)
							lastTime = currTime;
					});
					await Promise.all(userPromises);
					console.log("import end. last: %d", lastTime);
					fs.writeFileSync(
						"./last_tdavto.json",
						`${lastTime}`,
						"utf8"
					);
					axios.get(`https://api.telegram.org/bot1992463272:AAGl2ptLKNGAJN4kcKrTZB8pB6Cz8FfP0G8/sendMessage?chat_id=-559336176&text=Force%20import%20success.\nLast%20time:%20${lastTime.toLocaleString()}`);

				}
				return offset;
			},
		},

		/**
		 * Sync users with abcp
		 *
		 */
		userSync: {
			rest: "/user/sync",
			params: {},
			/** @param {Context} ctx  */
			async handler(ctx) {
				let last = new Date(parseInt(fs.readFileSync("./last_tdavto.json", "utf8")) + 1000); //last date +1 sec
				const format = `${ // YYYY-mm-dd HH:mm:ss
					last.getFullYear().toString().padStart(4, "0")}-${
					(last.getMonth()+1).toString().padStart(2, "0")}-${
					last.getDate().toString().padStart(2, "0")} ${
					last.getHours().toString().padStart(2, "0")}:${
					last.getMinutes().toString().padStart(2, "0")}:${
					last.getSeconds().toString().padStart(2, "0")}`;
				console.log(`last: ${format}`);
				const users = await axios
					.get("https://id9686.public.api.abcp.ru/cp/users", {
						params: {
							userlogin: set.userlogin,
							userpsw: set.userpsw,
							dateUpdatedStart: format
						}
					})
					.then(res => res.data)
					.catch((error) => {
						console.error(error);
						return error;
					});
				// console.log(users);
				if (users.length == 0) return 0;
				else {
					console.log("users", users);
					let currTime;
					const userPromises = users.map(async (user) => {
						const result = await ctx.call("v1.users.find", {
							query: {
								userId: user.userId
							}
						});

						if (result == 0) {
							// console.log("user not found");
							await ctx.call("v1.users.create", user);
						} else {
							// console.log(`user found: ${result[0]._id}`);
							// console.log(result[0]);
							user.id = result[0]._id;
							await ctx.call("v1.users.update", user);
						}
						currTime = new Date(user.updateTime);
						if (currTime > last)
							last = currTime;
					});

					await Promise.all(userPromises);
					fs.writeFileSync(
						"./last_tdavto.json",
						`${last.getTime()}`,
						"utf8"
					);
					axios.get(`https://api.telegram.org/bot1992463272:AAGl2ptLKNGAJN4kcKrTZB8pB6Cz8FfP0G8/sendMessage?chat_id=-559336176&text=Sync%20passed.\nCount:%20${users.length}\nLast%20time:%20${last.toLocaleString("ru")}`);
					return users.length;
				}
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
