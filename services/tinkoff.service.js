"use strict";

const axios = require("axios");
const md5 = require("md5");
const fs = require("fs");
const QueueMixin = require("moleculer-rabbitmq");
const Cron = require("@stretchshop/moleculer-cron");


const queueMixin = QueueMixin({
	connection: "amqp://admin:abcponelove@rabbitmq:5672",
	asyncActions: true, // Enable auto generate .async version for actions
});
// const accounts = require("accounts").Service;
// const { ServiceBroker } = require("moleculer");
// const  = new ServiceBroker();
// const service = require("../services/accounts.service");
// const broker = new ServiceBroker();
// const service = broker.createService(TestService);
// broker.loadService("./services/accounts.service");
// broker.start();
// const token = "t.MBsOaD8VuIVIhcayUuWcSC-OmY7OgCBNoB4R2s5wqQtHIHK8552nZYn9GlqsWwBYLM7rOFGnZJGABGFlR3fikw";
// const token = "t.K3VUSaBofcwD6MdBe3GByZVaNA734gFt81-rRVMa1vZ3-hn9cNMyJV19t1a2wJZWHYF88BWEtTa_Aa1adpLtIw"; //yry
const token = "t.nL4v8Qtp6Ebd0M1POeeGqR0TcEFmzeNUCyVwXrG9SrmQ2sYMb66qCydAxkW-0tipM6FQFKBxaaNSfpFzVinC6w"; //maria


/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "tinkoff",
	version: 1,
	mixins: [queueMixin, Cron],
	/**
	 * Settings
	 */
	settings: {
		amqp: {
			connection: "amqp://admin:abcponelove@rabbitmq:5672", // You can also override setting from service setting
		},
	},
	/**
	 * Dependencies
	 */
	dependencies: [],

	crons: [{
		name: "CheckInvoices",
		cronTime: '*/5 * * * *',
		onTick: function () {

			console.log('CheckInvoices ticked');

			this.getLocalService("tinkoff",1)
				.actions.invoices()
				.then((data) => {
					console.log("CheckInvoices:", data);
				});
		},
		runOnInit: function () {
			console.log("CheckInvoices is created");
		},
		timeZone: 'Europe/Moscow'
	}],

	/**
	 * Actions
	 */
	actions: {

		/**
		 * Redeems accounts from tinkoff account.
		 *
		 * @returns
		 */
		accounts: {
			rest: {
				method: "GET",
				path: "/accounts"
			},
			async handler(ctx) {
				let response;
				response = axios
					.get("https://business.tinkoff.ru/openapi/api/v3/bank-accounts", {
						headers: {
							"Authorization": `Bearer ${token}`
						}
					})
					.then(res => res.data)
					.catch(error => {
						console.error(error);
						return error;
					});
				let acc = await ctx.call("v1.accounts.find");
				console.log(acc);
				// acc.forEach(element => {
				// 	console.log(element);
				// 	ctx.call("v1.accounts.remove", {id: element._id});
				// });
				// if (!acc || acc.accountNumber != response.accountNumber) {
				// response = {"status": 1, "detail": "Account not found. Created new account"};
				// 	acc = await ctx.call("v1.accounts.create", response);
				// } else response = {"status": 1, "detail": "Account already exists"};

				return response;
				// console.log('defined');
				// return response;
			}
		},
		invoices: {
			rest: {
				method: "GET",
				path: "/invoices"
			},
			async handler(ctx) {
				// await this.waitForServices("abcp");
				let acc = {
						accountNumber: 40802810800002360307
					},
					md5,
					result,
					i_md5 = JSON.parse(fs.readFileSync("./f_md5_tdavto.json", "utf8"));

				let invoices = await axios
					.get(`https://business.tinkoff.ru/openapi/api/v1/bank-statement?accountNumber=40702810610000573512&from=2022-01-27`, {
						headers: {
							"Authorization": `Bearer ${token}`
						}
					})
					.then(res => res.data.operation)
					.catch(error => {
						console.error(error);
						return error.response;
					});
				let arr = await invoices.filter((inv) => {
					if (inv.payerInn == 4217196109) // исходящие платежи
						return false;
					if (inv.payerInn == 7710140679) // эквайринг и прочее (АО Тинькофф Банк)
						return false;
					if (i_md5[inv.operationId] != null) // обрабатывалось ранее
						return false;

					return true;
				});
				// for(let inv of invoices) console.log(inv);
				// return invoices.length;
				// console.log(`already proceeded`);
				await ctx.call("abcp.userSync");
				const invoicePromises = arr.map(async (inv) => {
					// if(!result.length) {
					//  	result = ctx.call("abcp.hello");
					// } else result.forEach((value) => {
					console.log(inv);
					// console.log(await ctx.call("v1.users.find", {query: {inn: inv.payerInn}}));
					console.log(await ctx.call("abcp.paymentNew.async", {
						params: inv
					}));
					i_md5[inv.operationId] = 1;
				});

				invoices = await Promise.all(invoicePromises);
				console.log('parsed all');
				if(arr.length)
					await ctx.call("v1.invoices.create", arr);
				fs.writeFileSync("f_md5_tdavto.json", JSON.stringify(i_md5), "utf8");
				return invoices.length;
				// console.log(response);
				// if(response.errorMessage) {
				// 	return `fatal error: ${response.errorMessage}`;
				// } else return response;
				// console.log('defined');
				// return response;
			}
		},

		/**
		 * Welcome, a username
		 *
		 * @param {String} name - User name
		 */
		welcome: {
			rest: "/welcome",
			params: {
				name: "string"
			},
			/** @param {Context} ctx  */
			async handler(ctx) {
				return `Welcome, ${ctx.params.name}`;
			}
		}
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	async stopped() {

	}
};
