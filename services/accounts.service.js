"use strict";

const DbMixin = require("../mixins/db.mixin");

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "accounts",
	version: 1,

	/**
	 * Mixins
	 */
	mixins: [DbMixin("accounts")],

	/**
	 * Settings
	 */
	settings: {
		// Available fields in the responses
		fields: [
			"_id",
			"accountNumber",
			"name",
			"currency",
			"bankBik",
			"accountType",
			"activationDate",
			"balance"
		],

		// Validator for the `create` & `insert` actions.
		entityValidator: {
			// accountNumber: "int|positive"
		}
	},

	/**
	 * Action Hooks
	 */
	hooks: {
		before: {
			/**
			 * Register a before hook for the `create` action.
			 * It sets a default value for the quantity field.
			 *
			 * @param {Context} ctx
			 */
			create(ctx) {
				// ctx.params.partner_id = 0;
				// ctx.params.account_id = 0;
				// ctx.params.disabled = 0;
			}
		}
	},

	/**
	 * Actions
	 */
	actions: {
	},

	/**
	 * Methods
	 */
	methods: {
	},

	/**
	 * Fired after database connection establishing.
	 */
	async afterConnected() {
		// await this.adapter.collection.createIndex({ name: 1 });
	}
};
